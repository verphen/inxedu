package com.inxedu.os.edu.constants.enums;

/**
 * @description
 * @author : www.inxedu.com
 */
public enum PayType {
    ALIPAY,//支付宝
    FREE//免费赠送,后台赠送
}
