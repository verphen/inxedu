package com.inxedu.os.edu.service.mobile;


/**
 * 
 * @ClassName  com.inxedu.os.edu.service.user.SmsThread
 * @description
 * @author :xujunbao
 * @Create Date : 2014年9月22日 下午5:32:52
 */
public class SmsThread extends Thread{
    private final SmsServiceStub sendSms;

    public SmsThread(SmsServiceStub sendSms) {
        this.sendSms = sendSms;
    }

    @Override
    public void run() {
        sendSms.sendmsg();
    }
}
